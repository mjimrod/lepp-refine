# Autor: Mitchel Jimenez
import numpy as np
import matplotlib.pyplot as plt
from time import sleep

# Clase Triangle: son los triangulos. Cada triangulo tiene asociado 3 vertices
# que lo conforman y ademas los 3 triangulos vecinos posible que tiene.
# Ademas, le colocamos un indice que sera su posicion en el arreglo de la Clase
# Triangulation. Finalmente, un marcador para saber rapidamente si el triangulo
# es malo o bueno.
class Triangle:
    def __init__(self, vertexAssociated = [], nearTriangles = []):
        self.vertexAssociated = vertexAssociated # Lista de vertices que lo conforman
        self.nearTriangles = nearTriangles # Lista de triangulos vecinos
        self.index = 0 # Indice en el arreglo de la clase Triangulation

    # Con esta funcion recibimos un triangulo y dos vertices que pertenecen a
    # ese triangulo y encontramos el indice restante del triangulo
    def findOtherVertex(self, vertex1, vertex2):
        for i in range(3):
            v = self.vertexAssociated[i]
            if (not v.isEqual(vertex1)) and (not v.isEqual(vertex2)):
                return v
        # Esto no deberia pasar puesto que no deberian crearse triangulos
        # con 2 o 3 puntos iguales, que de por si, no son triangulos,
        # siempre deberia encontrarse algun v que cumpla con tal condicion
        return None

    # Esta funcion nos entrega el indice en la lista vertexAssociated que tiene
    # un cierto vertice. Retorna None si el vertice no esta en ese triangulo
    def giveMeIndex(self, vertex):
        for i in range(3):
            if vertex.isEqual(self.vertexAssociated[i]):
                return i
        return None

    # Esta funcion encuentra la arista mas larga y retorno la misma arista
    # con el indice del vertice al que esta opuesta, esto para poder navegar
    # mas facilmente al triangulo vecino
    def longestEdge(self):
        # Obtengo mis puntos
        a = np.array([self.vertexAssociated[0].x, self.vertexAssociated[0].y])
        b = np.array([self.vertexAssociated[1].x, self.vertexAssociated[1].y])
        c = np.array([self.vertexAssociated[2].x, self.vertexAssociated[2].y])

        # Vectores
        ab = b - a
        bc = c - b
        ca = a - c

        # Calculo las normas
        longAB = np.linalg.norm(ab)
        longBC = np.linalg.norm(bc)
        longCA = np.linalg.norm(ca)

        # Calculo el maximo
        maximo = max(longAB, longBC, longCA)

        # Decido y entrego
        if maximo == longAB:
            return Edge([self.vertexAssociated[0], self.vertexAssociated[1]]), 2
        elif maximo == longBC:
            return Edge([self.vertexAssociated[1], self.vertexAssociated[2]]), 0
        else:
            return Edge([self.vertexAssociated[2], self.vertexAssociated[0]]), 1

        return None

    # Funcion que verifica si un triangulo es bueno
    def isBad(self):
        # Angulo minimo
        minTheta = np.pi/6

        # Tengo los vertices
        a = np.array([float(self.vertexAssociated[0].x), float(self.vertexAssociated[0].y)])
        b = np.array([float(self.vertexAssociated[1].x), float(self.vertexAssociated[1].y)])
        c = np.array([float(self.vertexAssociated[2].x), float(self.vertexAssociated[2].y)])

        # Tengo los vectores
        ab = b - a
        bc = c - b
        ca = a - c

        longAB = np.linalg.norm(ab)
        longBC = np.linalg.norm(bc)
        longCA = np.linalg.norm(ca)

        # Tengo los angulos
        theta = [np.arccos((longCA**2 + longBC**2 - longAB**2)/(2 * longCA * longBC)),
                  np.arccos((longAB**2 + longBC**2 - longCA**2)/(2 * longAB * longBC)),
                  np.arccos((longAB**2 + longCA**2 - longBC**2)/(2 * longAB * longCA))]
        if min(theta) < minTheta:
            return True
        return False

# Clase Vertex: son los vertices. Tienen dos valores reales asociados a la pos.
class Vertex:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    # Compara dos vertices para saber si son iguales
    def isEqualv(self, vertex):
        if self.x == vertex.x and self.y == vertex.y:
            return True
        return False

    # Revisa si un punto esta dentro del circulo, esta sobre el circulo o fuera
    def inCircle(self, triangle):
        assert type(triangle) == Triangle

        # Vertices a usar
        a = triangle.vertexAssociated[0]
        b = triangle.vertexAssociated[1]
        c = triangle.vertexAssociated[2]

        # Matriz a calcularle el determinante
        matrix = np.array([[a.x, a.y, a.x**2 + a.y**2, 1],
                           [b.x, b.y, b.x**2 + b.y**2, 1],
                           [c.x, c.y, c.x**2 + c.y**2, 1],
                           [self.x, self.y, self.x**2 + self.y**2, 1]])
        determinante = np.linalg.det(matrix)
        if determinante > 0:
            return True
        return False

# Clase Arista: hace mas facil el trabajo con las aristas.
class Edge:
    def __init__(self, vertices = []):
        self.vertices = vertices

    # Compruebo si dos aristas son iguales
    def isEqual(self, e):
        assert type(e) == Edge

        # Compruebo que vertices de las aristas sean iguales
        if (self.vertices[0].isEqualv(e.vertices[0]) and self.vertices[1].isEqualv(e.vertices[1])) ^ (self.vertices[0].isEqualv(e.vertices[1]) and self.vertices[1].isEqualv(e.vertices[0])):
            return True
        # Si no, retorno false
        return False

# Clase de la triangulacion: es basicamente un arreglo de triangulos y un
# arreglo de vertices, ademas de las coordenada maxima permitida
class Triangulation:
    def __init__(self):
        self.vertexList = []
        self.triangleList = []
        self.interactive = ""

    # Anhado una triangulacion ya hecha a un nuevo objeto para manipularla
    def addTriangulation(self, triangulation):
        self.vertexList = triangulation.vertexList
        self.triangleList = triangulation.triangleList

    # Encuentro la arista compartida entre dos triangulos
    def findCommonEdge(self, triangleOne, triangleTwo):
        assert type(triangleOne) == Triangle
        assert type(triangleTwo) == Triangle

        # Asigno mis vertices a variables para que se clarifique el proceso
        cero = triangleOne.vertexAssociated[0]
        uno = triangleOne.vertexAssociated[1]
        dos = triangleOne.vertexAssociated[2]

        tres = triangleTwo.vertexAssociated[0]
        cuatro = triangleTwo.vertexAssociated[1]
        cinco = triangleTwo.vertexAssociated[2]

        # Creo mis aristas
        edgesOne = [Edge([cero, uno]), Edge([uno, dos]), Edge([dos, cero])]
        edgesTwo = [Edge([tres, cuatro]), Edge([cuatro, cinco]), Edge([cinco, tres])]

        for i in range(3):
            for j in range(3):
                if edgesTwo[j].isEqual(edgesOne[i]):
                    return edgesOne[i]

        # Si no tienen arista comun, retorno None, pero no deberia pasar puesto
        # que siempre voy a usar dos triangulos vecinos
        return None

    # flipEdge simple
    def flipEdge(self, triangleOne, triangleTwo, commonEdge, indexV1, indexV3):
        assert type(triangleOne) == Triangle
        assert type(triangleTwo) == Triangle

        # Separo la arista en mis dos vertices
        v0 = commonEdge.vertices[0]
        v2 = commonEdge.vertices[1]

        # Extraigo los vertices restantes
        v1 = triangleOne.vertexAssociated[indexV1]
        v3 = triangleTwo.vertexAssociated[indexV3]

        # Tomo los triangulos vecinos de cada uno
        c0 = triangleOne.nearTriangles[triangleOne.giveMeIndex(v2)]
        c1 = triangleOne.nearTriangles[triangleOne.giveMeIndex(v0)]
        c2 = triangleTwo.nearTriangles[triangleTwo.giveMeIndex(v0)]
        c3 = triangleTwo.nearTriangles[triangleTwo.giveMeIndex(v2)]

        # Creo mis nuevos triangulos
        newTriangleOne = Triangle([v1, v2, v3])
        newTriangleTwo = Triangle([v3, v0, v1])

        # Actualizo los vecinos de los triangulos nuevos
        newTriangleOne.nearTriangles = [c2, newTriangleTwo, c1]
        newTriangleTwo.nearTriangles = [c0, newTriangleOne, c3]

        # Actualizo los nuevos vecinos de los triangulos vecinos
        if c0 != None:
            c0.nearTriangles[c0.giveMeIndex(c0.findOtherVertex(v0, v1))] = newTriangleTwo
        if c1 != None:
            c1.nearTriangles[c1.giveMeIndex(c1.findOtherVertex(v1, v2))] = newTriangleOne
        if c2 != None:
            c2.nearTriangles[c2.giveMeIndex(c2.findOtherVertex(v2, v3))] = newTriangleOne
        if c3 != None:
            c3.nearTriangles[c3.giveMeIndex(c3.findOtherVertex(v3, v0))] = newTriangleTwo

        # Actualizo los indices de los triangulos en el arreglo de triangulos
        # de la triangulacion
        newTriangleOne.index = triangleOne.index
        newTriangleTwo.index = triangleTwo.index

        # Reemplazo los triangulos viejos por los nuevos
        self.triangleList[newTriangleOne.index] = newTriangleOne
        self.triangleList[newTriangleTwo.index] = newTriangleTwo

        return None

    # Hace la recursion de lepp
    def lepp(self, t):
        assert type(t) == Triangle

        # Encuentro el longestEdge de t y el indice opuesto a el
        longestEdge, oppositeIndex = t.longestEdge()

        # Encuentro asi su vecino
        neighbor = t.nearTriangles[oppositeIndex]

        # Encuentro el longestEdge del vecino de t y el indice opuesto a ese
        if neighbor != None:
            neighborLongestEdge, neighborOppositeIndex = neighbor.longestEdge()
            if neighbor.nearTriangles[neighborOppositeIndex] == t:
                vt = t.vertexAssociated[oppositeIndex]
                vn = neighbor.vertexAssociated[neighborOppositeIndex]
                if vt.inCircle(neighbor) == True:
                    return self.flipEdge(neighbor, t, longestEdge, neighborOppositeIndex, oppositeIndex)
                elif vn.inCircle(t) == True:
                    return self.flipEdge(t, neighbor, longestEdge, oppositeIndex, neighborOppositeIndex)
                else:
                    return None
        else:
            return None
        # Caso recursivo, avanzamos al triangulo neighbor
        return self.lepp(neighbor)

    # Funcion que me recorre la triangulacion y procesa todo triangulo malo que
    # vea
    def processTriangulation(self):
        for i in range(len(self.triangleList)):
            t = self.triangleList[i]
            # Vemos si el triangulo es malo y si es malo, le aplicamos lepp
            if t != None and t.isBad():
                self.lepp(t)

    # Dibuja la triangulacion
    def drawRefinedTriangulation(self, z):
        fig, axes = plt.subplots()
        axes.set(title=f"Refined triangulation {z}", xticks=[], yticks=[])
        fig.set_size_inches(18.5, 10.5)
        fig.set_dpi(120)
        # Dibujamos el triangulo creado
        for i in range(len(self.triangleList)):
            triangle = self.triangleList[i]
            if triangle == None:
                continue
            for j in range(4):
                v = triangle.vertexAssociated[j % 3]
                v1 = triangle.vertexAssociated[(j+1) % 3]
                plt.plot([v.x, v1.x], [v.y, v1.y], 'k')
        plt.savefig(f'ref{z}.png')
